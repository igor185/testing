import CartParser from './CartParser';

const cards = "Product name,Price,Quantity\n" +
    "Mollis consequat,9.00,2\n" +
    "Tvoluptatem,10.32,1\n" +
    "Scelerisque lacinia,18.90,1\n" +
    "Consectetur adipiscing,28.72,10\n" +
    "Condimentum aliquet,13.90,1";


let parser, validate, parse, calcTotal, parseLine;


beforeEach(() => {
    parser = new CartParser();
    validate = parser.validate.bind(parser);
    parse = parser.parse.bind(parser);
    calcTotal = parser.calcTotal;
    parseLine = parser.parseLine.bind(parser);
});

describe("CartParser - unit tests", () => {
    it('should parseLine and return an object with match object', () => {
        // given
        let str = 'Consectetur adipiscing,28.72,10';
        // when
        let parse_line = parseLine(str);
        //then
        expect(parse_line).toMatchObject({
            "name": "Consectetur adipiscing",
            "price": 28.72,
            "quantity": 10
        })
    });

    it('should calcTotal and equal to expected total', () => {
        //given
        let items = [
            {
                "id": "3e6def17-5e87-4f27-b6b8-ae78948523a9",
                "name": "Mollis consequat",
                "price": 9,
                "quantity": 2
            },
            {
                "id": "90cd22aa-8bcf-4510-a18d-ec14656d1f6a",
                "name": "Tvoluptatem",
                "price": 10.32,
                "quantity": 1
            },
            {
                "id": "33c14844-8cae-4acd-91ed-6209a6c0bc31",
                "name": "Scelerisque lacinia",
                "price": 18.9,
                "quantity": 1
            },
            {
                "id": "f089a251-a563-46ef-b27b-5c9f6dd0afd3",
                "name": "Consectetur adipiscing",
                "price": 28.72,
                "quantity": 10
            },
            {
                "id": "0d1cbe5e-3de6-4f6a-9c53-bab32c168fbf",
                "name": "Condimentum aliquet",
                "price": 13.9,
                "quantity": 1
            }
        ];
        // then
        expect(calcTotal(items)).toBeCloseTo(348.32, 2);
    });

    it('should parse cards from variable and return match object', () => {
        //given
        parser.readFile = () => cards;
        parser.validate = () => [];
        // cards and cardsJson

        //when
        const parsedCards = parse();

        //then
        expect(parsedCards).toMatchObject({
            "items": [{
                "name": "Mollis consequat",
                "price": 9,
                "quantity": 2
            }, {
                "name": "Tvoluptatem",
                "price": 10.32,
                "quantity": 1
            }, {
                "name": "Scelerisque lacinia",
                "price": 18.9,
                "quantity": 1
            }, {
                "name": "Consectetur adipiscing",
                "price": 28.72,
                "quantity": 10
            }, {
                "name": "Condimentum aliquet",
                "price": 13.9,
                "quantity": 1
            }],
            "total": 348.32
        });
    });

    it('should throw error while parse invalid cards', () => {
        //given
        parser.readFile = () => cards;
        parser.validate = () => ['not empty array'];
        // cards and cardsJson

        //then
            expect(() => parse()).toThrowError();
    });

    it('should throw error if other header', () => {
        // given
        const brokenCards = "Product name,Pr,Quantity\n" +
            "Mollis consequat,9.00,2";
        //then
            expect(validate(brokenCards)).not.toEqual([]);
    });

    it('should throw error if wrong amount of headers', () => {
        // given
        const brokenHeaderAmount = "Product name,Quantity\n" +
            "Mollis consequat,9.00,2";
        //then
            expect(validate(brokenHeaderAmount)).not.toEqual([]);
    });

    it('should throw error if wrong quantity', () => {
        // given
        const brokenQuantity = "Product name,Quantity\n" +
            "Mollis consequat,9.00,'2'";
        //then
            expect(validate(brokenQuantity)).not.toEqual([]);
    });

    it('should throw error if one extra header', () => {
        // given
        const brokenHeaderAmount = "Product name,Price,Quantity, something\n" +
            "Mollis consequat,9.00,2";
        //then
        expect(() => validate(brokenHeaderAmount)).not.toEqual([]);
    });

    it('should throw error if no file', () => {
        //given
        parser.readFile = () => null;

        //when

        //then
        expect(() => parse()).toThrowError();
    });

    it('should throw error if no column required', () => {
        // given
        const brokenHeaderAmount = "Product name,Price\n" +
            "Mollis consequat,9.00";
        //then
        expect(validate(brokenHeaderAmount)).not.toEqual([]);
    });

    it('should return empty array on valid data', () => {
        //cards
        expect(validate(cards)).toEqual([]);
    });
});


describe("CartParser - integration tests", () => {
    it('should return a json object from card.csv', () => {
        // when
        let jsonObj = parse('./samples/cart.csv');
        // then
        expect(jsonObj).toMatchObject({
            "items": [{
                "name": "Mollis consequat",
                "price": 9,
                "quantity": 2
            }, {
                "name": "Tvoluptatem",
                "price": 10.32,
                "quantity": 1
            }, {
                "name": "Scelerisque lacinia",
                "price": 18.9,
                "quantity": 1
            }, {
                "name": "Consectetur adipiscing",
                "price": 28.72,
                "quantity": 10
            }, {
                "name": "Condimentum aliquet",
                "price": 13.9,
                "quantity": 1
            }],
            "total": 348.32
        });
    });
});

